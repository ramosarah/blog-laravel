<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
/**Les articles vont être aussi identifiables avec des étiquettes, 
 * ce qui permet une sélection transversale alors que 
 * les catégories créent une organisation verticale */

class Tag extends Model
{
    protected $fillable = ['tag'];
    public $timestamps = false;

    public function posts()
    {
    return $this->belongsToMany(Post::class);
    }
}
