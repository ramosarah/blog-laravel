<?php
namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\{ Category, Page, Follow };

class HomeComposer
{
  /**
   * Bind data to the view.
   *
   * @param  View  $view
   * @return void
   */
  public function compose(View $view)
  {
    $view->with([
      'categories'  => Category::has('posts')->get(), 
      //On a déjà créer un composeur de vue pour le layout (app\Http\ViewComposers\HomeComposer) pour envoyer systématiquement les catégories. On va aussi envoyer les pages :
      'pages'       => Page::select('slug', 'title')->get(),
      //On va compléter HomeComposer pour envoyer systématiquement les liens sociaux :
      'follows'    => Follow::all(),
      ]);
  }
}