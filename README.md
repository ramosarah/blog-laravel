# Blog with Laravel 8

## Requirements
- Composer v2
- Docker and docker-compose 
- npm v6


## How to clone
Copy "clone with ssh" link, then:
```sh
git clone git@gitlab.com:ramosarah/blog-laravel.git
```

## Install Composer and npm Dependencies
```sh
cd blog-laravel
composer install
npm install
npm run dev
```

## Create a copy of your .env file
```sh
cp .env.example .env
```

## Key encryption, Server and Database
You can now run the serve, generate an encryption key and make a migration
```sh
php artisan key:generate
php artisan serve
php artisan migrate
```